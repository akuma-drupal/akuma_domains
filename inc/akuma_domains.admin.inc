<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Admin page callbacks for the Akuma Core module.
 */

/**
 * Form builder; Configure settings.
 *
 * @ingroup akuma_domains
 *
 * @see     system_settings_form()
 */
function akuma_domains_admin_settings_form($form, $form_state) {
  drupal_set_title(t('Akuma: Domains'));

  $form['domain_langs'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Domain Language Options'),
  );
  $form['domain_langs']['domain_overrides_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable domain overrides'),
    '#default_value' => variable_get('domain_overrides_enabled', FALSE),
    '#description'   => t('Check this box to add support to override language domain.'),
  );

  $form['#submit'][] = 'akuma_domains_admin_settings_form_submit';

  return system_settings_form($form);
}

// Submit callback.
function akuma_domains_admin_settings_form_submit($form, &$form_state) {

}