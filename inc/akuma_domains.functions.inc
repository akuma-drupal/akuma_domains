<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Admin page callbacks for the Akuma Core module.
 */


/**
 * Returns first found domain from config
 *
 * @param      $lang
 * @param null $default
 *
 * @return null|string
 */
function lang_get_domain($lang, $default = null)
{
    $domain_overrides = isset($GLOBALS['conf']['akuma_domains']) ? $GLOBALS['conf']['akuma_domains'] : array();
    foreach ($domain_overrides as $domain => $language) {
        if ($language == $lang) {
            return $domain;
        }
    }

    return $default;
}

/**
 * @param null $domain
 *
 * @return null
 */
function akuma_domains_configure_language($domain = null)
{
    if (is_null($domain)) {
        $domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : null;
    }

    /** Try to negotiate overridden language */
    $dom_lang = domain_get_lang($domain);

    if (!$dom_lang) {
        return null;
    }

    // Initialize the language list now. There is no harm in doing this now as
    // opposed to waiting for the language bootstrap level since the function
    // will always load the language list directly from the db anyway.
    language_list('language');

    // Override language domains directly in the language list static cache.
    $languages = & drupal_static('language_list');

    if (!is_array($languages) || !isset($languages['language']) || !isset($languages['language'][$dom_lang])) {
        return null;
    }

    $languages['language'][$dom_lang]->domain = $domain;

    // Override domain for any initialized GLOBAL language type variables.
    foreach (language_types() as $type) {
        if (!empty($GLOBALS[$type])) {
            if (strcmp(strtolower($GLOBALS[$type]->language), strtolower($dom_lang)) === 0) {
                $GLOBALS[$type]->domain = $domain;
            };
        }
    }

    return $dom_lang;
}

/**
 * Helper function to negotiate overridden domain language,
 * returns found overridden language code or value specified in $default
 *
 * @param null $domain
 * @param null $default
 *
 * @return null
 */
function domain_get_lang($domain = null, $default = null)
{
    if (is_null($domain)) {
        $domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : null;
    }

    if (!$domain) {
        return $default;
    }

    $domain_overrides = $GLOBALS['conf']['akuma_domains'];

    return isset($domain_overrides[$domain]) ? $domain_overrides[$domain] : $default;
}
